package com.company.laba_4.controller;

import com.company.laba_4.controller.wrapperxsl.ArticleXSL;
import com.company.laba_4.entity.Article;
import com.company.laba_4.model.ArticlesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping("article")
public class ArticleController {

    @Autowired
    private ArticlesDAO articlesDAO;

    @GetMapping(produces = { APPLICATION_JSON_VALUE,
            APPLICATION_XML_VALUE})
    public Object articles()  {
        return new ArticleXSL(articlesDAO.allArticles());
    }

    @GetMapping(value = "{id}", produces = { APPLICATION_JSON_VALUE,
            APPLICATION_XML_VALUE})
    public Object findById(@PathVariable int id) {
        return articlesDAO.findById(id);
    }

    @DeleteMapping("{id}")
    public void removeArticle(@PathVariable int id) {
        articlesDAO.removeArticle(articlesDAO.findById(id));
    }

    @PostMapping
    public void createArticle(@RequestBody Article articles) {
        articlesDAO.addArticle(articles);
    }

    @PutMapping
    public void editArticle(@RequestBody Article articles) {
        articlesDAO.editArticle(articles);
    }

}
