package com.company.laba_4.controller;

import com.company.laba_4.controller.wrapperxsl.AuthorXSL;
import com.company.laba_4.entity.Author;
import com.company.laba_4.model.AuthorsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping("author")
public class AuthorController {

    @Autowired
    private AuthorsDAO authorsDAO;

    @GetMapping(produces = { APPLICATION_JSON_VALUE,
            APPLICATION_XML_VALUE})
    public Object authors()  {
        return  new AuthorXSL(authorsDAO.allAuthors());
    }

    @GetMapping(value = "{id}", produces = { APPLICATION_JSON_VALUE,
            APPLICATION_XML_VALUE})
    public Object findById(@PathVariable int id) {
        return  authorsDAO.findById(id);
    }

    @DeleteMapping("{id}")
    public void removeAuthor(@PathVariable int id) {
        authorsDAO.removeAuthor(authorsDAO.findById(id));
    }

    @PostMapping
    public void createAuthor(@RequestBody Author authors) {
        authorsDAO.addAuthor(authors);
    }

    @PutMapping
    public void editAuthor(@RequestBody Author authors) {
        authorsDAO.editAuthor(authors);
    }

}
