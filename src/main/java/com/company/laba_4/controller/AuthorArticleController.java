package com.company.laba_4.controller;

import com.company.laba_4.controller.wrapperxsl.ArticleXSL;
import com.company.laba_4.controller.wrapperxsl.AuthorArticleXSL;
import com.company.laba_4.entity.AuthorArticle;
import com.company.laba_4.model.AuthorArticleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping("author-article")
public class AuthorArticleController {

    @Autowired
    private AuthorArticleDAO authorArticleDAO;

    @GetMapping(produces = { APPLICATION_JSON_VALUE,
            APPLICATION_XML_VALUE})
    public Object authorArticle()  {
        return new AuthorArticleXSL(authorArticleDAO.allAuthorArticle());
    }

    @GetMapping(value = "{id}", produces = { APPLICATION_JSON_VALUE,
            APPLICATION_XML_VALUE})
    public Object findById(@PathVariable int id) {
        return authorArticleDAO.findById(id);
    }

    @DeleteMapping("{id}")
    public void removeAuthorArticle(@PathVariable int id) {
        authorArticleDAO.removeAuthorArticle(authorArticleDAO.findById(id));
    }

    @PostMapping
    public void createAuthorArticle(@RequestBody AuthorArticle authorArticle) {
        authorArticleDAO.addAuthorArticle(authorArticle);
    }

    @PutMapping
    public void editAuthorArticle(@RequestBody AuthorArticle authorArticle) {
        authorArticleDAO.editAuthorArticle(authorArticle);
    }

}
