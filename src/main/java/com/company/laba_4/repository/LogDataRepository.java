package com.company.laba_4.repository;

import com.company.laba_4.entity.LogData;
import org.springframework.data.repository.CrudRepository;

public interface LogDataRepository  extends CrudRepository<LogData, Long> {
}