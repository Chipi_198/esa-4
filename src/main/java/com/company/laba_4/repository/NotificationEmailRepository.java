package com.company.laba_4.repository;

import com.company.laba_4.entity.NotificationEmail;
import org.springframework.data.repository.CrudRepository;

public interface NotificationEmailRepository extends CrudRepository<NotificationEmail, Long> {
}
