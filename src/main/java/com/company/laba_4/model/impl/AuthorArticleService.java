package com.company.laba_4.model.impl;

import com.google.common.collect.Lists;
import com.company.laba_4.entity.AuthorArticle;
import com.company.laba_4.jms.SenderMessage;
import com.company.laba_4.model.AuthorArticleDAO;
import com.company.laba_4.repository.AuthorArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthorArticleService implements AuthorArticleDAO {
    @Autowired
    private AuthorArticleRepository authorArticleRepository;

    @Override
    public void addAuthorArticle(AuthorArticle authorArticle) {
        SenderMessage.send("addAuthorArticle " + authorArticle.toString());
        authorArticleRepository.save(authorArticle);
    }

    @Override
    public void removeAuthorArticle(AuthorArticle authorArticle) {
        SenderMessage.send("removeAuthorArticle " + authorArticle.toString());
        authorArticleRepository.delete(authorArticle);
    }

    @Override
    public void editAuthorArticle(AuthorArticle authorArticle) {
        SenderMessage.send("editAuthorArticle " + authorArticle.toString());
        authorArticleRepository.save(authorArticle);
    }

    @Override
    public AuthorArticle findById(Integer id) {
        return authorArticleRepository.findById(id).get();
    }

    @Override
    public List<AuthorArticle> allAuthorArticle() {
        return Lists.newArrayList(authorArticleRepository.findAll());
    }
}
