package com.company.laba_4.model.impl;

import com.google.common.collect.Lists;
import com.company.laba_4.entity.Author;
import com.company.laba_4.jms.SenderMessage;
import com.company.laba_4.model.AuthorsDAO;
import com.company.laba_4.repository.AuthorsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthorsService implements AuthorsDAO {
    @Autowired
    private AuthorsRepository authorsRepository;

    @Override
    public void addAuthor(Author author) {
        SenderMessage.send("addAuthor " + author.toString());
        authorsRepository.save(author);
    }

    @Override
    public void removeAuthor(Author author) {
        SenderMessage.send("removeAuthor " + author.toString());
        authorsRepository.delete(author);
    }

    @Override
    public void editAuthor(Author author) {
        SenderMessage.send("editAuthor " + author.toString());
        authorsRepository.save(author);
    }

    @Override
    public Author findById(Integer id) {
        return authorsRepository.findById(id).get();
    }

    @Override
    public List<Author> allAuthors() {
        return Lists.newArrayList(authorsRepository.findAll());
    }
}
