package com.company.laba_4.model.impl;

import com.google.common.collect.Lists;
import com.company.laba_4.entity.Article;
import com.company.laba_4.jms.SenderMessage;
import com.company.laba_4.model.ArticlesDAO;
import com.company.laba_4.repository.ArticlesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ArticlesService implements ArticlesDAO {
    @Autowired
    private ArticlesRepository articlesRepository;

    @Override
    public void addArticle(Article article) {
        SenderMessage.send("addArticle " + article.toString());
        articlesRepository.save(article);
    }

    @Override
    public void removeArticle(Article article) {
        SenderMessage.send("removeArticle " + article.toString());
        articlesRepository.delete(article);
    }

    @Override
    public void editArticle(Article article) {
        SenderMessage.send("editArticle " + article.toString());
        articlesRepository.save(article);
    }

    @Override
    public Article findById(Integer id) {
        return articlesRepository.findById(id).get();
    }

    @Override
    public List<Article> allArticles() {
        return Lists.newArrayList(articlesRepository.findAll());
    }
}
