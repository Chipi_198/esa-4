package com.company.laba_4.model;

import com.company.laba_4.entity.AuthorArticle;

import java.util.List;

public interface AuthorArticleDAO {
    void addAuthorArticle(AuthorArticle authorArticle);
    void removeAuthorArticle(AuthorArticle authorArticle);
    void editAuthorArticle(AuthorArticle authorArticle);
    AuthorArticle findById(Integer id);
    List<AuthorArticle> allAuthorArticle();
}
