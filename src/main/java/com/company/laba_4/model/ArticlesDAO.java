package com.company.laba_4.model;

import com.company.laba_4.entity.Article;

import java.util.List;

public interface ArticlesDAO {
    void addArticle(Article article);
    void removeArticle(Article article);
    void editArticle(Article article);
    Article findById(Integer id);
    List<Article> allArticles();
}
