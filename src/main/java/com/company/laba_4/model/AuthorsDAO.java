package com.company.laba_4.model;

import com.company.laba_4.entity.Author;

import java.util.List;

public interface AuthorsDAO {
    void addAuthor(Author author);
    void removeAuthor(Author author);
    void editAuthor(Author author);
    Author findById(Integer id);
    List<Author> allAuthors();
}
