package com.company.laba_4.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "author_article", schema = "public")
public class AuthorArticle {
    private Integer id;
    private Integer idArticle;
    private Integer idAuthor;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_article")
    public Integer getIDArticle() {
        return idArticle;
    }

    public void setIDArticle(Integer idArticle) {
        this.idArticle = idArticle;
    }

    @Basic
    @Column(name = "id_author")
    public Integer getIDAuthor() {
        return idAuthor;
    }

    public void setIDAuthor(Integer idAuthor) {
        this.idAuthor = idAuthor;
    }

    @Override
    public String toString() {
        return "Author {" +
                "id=" + id +
                ", id_article='" + idArticle + '\'' +
                ", id_author='" + idAuthor + '\'' +
                '}';
    }


}