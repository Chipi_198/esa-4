package com.company.laba_4.jms;

import com.company.laba_4.entity.LogData;
import com.company.laba_4.entity.NotificationEmail;
import com.company.laba_4.repository.LogDataRepository;
import com.company.laba_4.repository.NotificationEmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

import java.util.ArrayList;

import static com.company.laba_4.jms.SenderMessage.generateAndSendEmail;

@Component
public class ReceiveMessage {

    @Autowired
    private LogDataRepository logDataRepository;
    @Autowired
    private NotificationEmailRepository notificationEmailRepository;

    @JmsListener(destination = "sampleQueue")
    public void receiveMessage(String massage) {
        ArrayList<String> email;
        String[] words = massage.split("\\s");
        LogData data = new LogData();
        data.setClassname(words[1]);
        data.setTypechange(words[0]);
        data.setValue(massage);
        logDataRepository.save(data);
        for ( NotificationEmail m:notificationEmailRepository.findAll()) {
            if(words[0].equals(m.getCondition())) {
                sendEmail(massage,m.getEmail());
                System.out.println("Email :" + m.getEmail());
            }
        }

        System.out.println("Received :" + massage);
    }

    private void sendEmail(String massage, String email) {
        try {
            generateAndSendEmail(massage, email);
        } catch (MessagingException e) {
            System.out.println("Error - generateAndSendEmail");
        }
    }
}